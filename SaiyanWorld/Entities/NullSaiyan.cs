﻿namespace SaiyanWorld.Entities
{
    public class NullSaiyan : ISaiyan
    {
        public string Name => "Not Available in Saiyan Database";
        public int Power => 0;

        public override string ToString() => Name;
    }
}
