﻿namespace SaiyanWorld.Entities
{
    public interface ISaiyan
    {
        public string Name { get; }

        public int Power { get; }
    }
}
