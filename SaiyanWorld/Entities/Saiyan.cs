﻿namespace SaiyanWorld.Entities
{
    public class Saiyan : ISaiyan
    {
        public Saiyan(string name, int power)
        {
            Name = name;
            Power = power;
        }

        public string Name { get; private set; }
        public int Power { get; private set; }

        public override string ToString() => $"{Name} - {Power}";
    }
}