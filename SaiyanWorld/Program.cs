﻿using System;
using SaiyanWorld.Services;
using SaiyanWorld.Views;

namespace SaiyanWorld
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var saiyanService = new SaiyanService();
            var saiyan = saiyanService.GetSaiyan("Vegeta");

            var view = new  SaiyanView(saiyan);
            view.RenderView();
        }
    }
}
