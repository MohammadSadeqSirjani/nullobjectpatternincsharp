﻿using SaiyanWorld.Entities;
using System;

namespace SaiyanWorld.Views
{
    public class SaiyanView
    {
        private readonly ISaiyan _saiyan;

        public SaiyanView(ISaiyan saiyan)
        {
            _saiyan = saiyan;
        }

        public void RenderView()
        {
            Console.WriteLine(_saiyan);
        }
    }
}
