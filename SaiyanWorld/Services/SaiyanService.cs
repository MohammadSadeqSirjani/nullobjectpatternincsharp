﻿using SaiyanWorld.Entities;
using System.Collections.Generic;
using System.Linq;

namespace SaiyanWorld.Services
{
    public class SaiyanService
    {
        private readonly SaiyanRepository _saiyanRepository = new SaiyanRepository();

        public ISaiyan GetSaiyan(string name)
        {
            var saiyan = _saiyanRepository.GetSaiyan(name);

            return saiyan ?? new NullSaiyan();
        }

        private class SaiyanRepository
        {
            private readonly IList<Saiyan> _saiyan;

            public SaiyanRepository()
            {
                _saiyan = new List<Saiyan>()
                {
                    new Saiyan("Son Goku", 1000),
                    new Saiyan("Soon Gohan", 800),
                    new Saiyan("Vegeta", 950)
                };
            }

            public ISaiyan GetSaiyan(string name)
            {
                var saiyanExists = _saiyan.Any(s => s.Name == name);

                return saiyanExists ? _saiyan.FirstOrDefault(s => s.Name == name) : null;
            }
        }
    }
}
