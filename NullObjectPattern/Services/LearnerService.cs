﻿using NullObjectPattern.Entities;
using System.Collections.Generic;
using System.Linq;

namespace NullObjectPattern.Services
{
    public class LearnerService
    {
        private readonly LearnerRepository _learnerRepository = new LearnerRepository();

        public ILearner GetCurrentLearner()
        {
            const int learnerId = -1;

            var learner = _learnerRepository.GetLearner(learnerId);

            return learner ?? new NullLearner();
        }

        private class LearnerRepository
        {
            private readonly IList<Learner> _learners = new List<Learner>();

            public LearnerRepository()
            {
                _learners.Add(new Learner(1, "David", 83));
                _learners.Add(new Learner(2, "Julie", 72));
                _learners.Add(new Learner(3, "Scott", 92));
            }

            public ILearner GetLearner(int id)
            {
                var learnerExists = _learners.Any(l => l.Id == id);

                return learnerExists ? _learners.FirstOrDefault(l => l.Id == id) : null;
            }
        }
    }
}
