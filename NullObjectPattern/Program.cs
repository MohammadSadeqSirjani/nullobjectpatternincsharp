﻿using NullObjectPattern.Services;
using NullObjectPattern.Views;

namespace NullObjectPattern
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var learnerService = new LearnerService();
            var learner = learnerService.GetCurrentLearner();

            var view = new LearnerView(learner);
            view.RenderView();
        }
    }
}
