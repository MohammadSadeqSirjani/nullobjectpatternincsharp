﻿using NullObjectPattern.Entities;
using System;

namespace NullObjectPattern.Views
{
    public class LearnerView
    {
        private readonly ILearner _learner;

        public LearnerView(ILearner learner)
        {
            _learner = learner;
        }

        public void RenderView()
        {
            Console.WriteLine($"Username: {_learner.Username}");
            Console.WriteLine($"Courses Completed: {_learner.CoursesCompleted}");
        }
    }
}
