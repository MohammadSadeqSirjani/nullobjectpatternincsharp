﻿namespace NullObjectPattern.Entities
{
    public interface ILearner
    {
        public int Id { get; }

        public string Username { get; }

        public int CoursesCompleted { get; }
    }
}