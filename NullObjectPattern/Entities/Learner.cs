﻿namespace NullObjectPattern.Entities
{
    public class Learner : ILearner
    {
        public Learner(int id, string username, int coursesCompleted)
        {
            Id = id;
            Username = username;
            CoursesCompleted = coursesCompleted;
        }

        public int Id { get; private set; }

        public string Username { get; private set; }

        public int CoursesCompleted { get; private set; }
    }
}
