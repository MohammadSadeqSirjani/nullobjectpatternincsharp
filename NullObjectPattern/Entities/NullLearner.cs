﻿namespace NullObjectPattern.Entities
{
    public class NullLearner : ILearner
    {
        public int Id => -1;
        public string Username => "Just Browsing";
        public int CoursesCompleted => 0;
    }
}
